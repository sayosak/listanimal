package com.example.listanimals;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Q1
        //listView
        final ListView animalList = (ListView) findViewById(R.id.animalList);

        //Get the Key array of the animalList
        final String[] animalsNames = AnimalList.getNameArray();

        //ArrayAdapter
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, animalsNames);

        //set the adapter on the animalList
        animalList.setAdapter(adapter);

        //Q2
        animalList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                String clickedAnimal = animalsNames[position];

                Intent intent = new Intent(MainActivity.this, ClickedAnimal.class);

                //stock animalName
                intent.putExtra("animalName", clickedAnimal);
                startActivity(intent);
            }
        });
    }
}




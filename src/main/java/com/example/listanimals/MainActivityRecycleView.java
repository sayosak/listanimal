package com.example.listanimals;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivityRecycleView extends AppCompatActivity {
    private static final String[] animalsNames = AnimalList.getNameArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        final RecyclerView rv = (RecyclerView) findViewById(R.id.animalList);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new MyAdapter());
    }

    class MyAdapter extends RecyclerView.Adapter<RowHolder> {

        @NonNull
        @Override
        public RowHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return(new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));
        }

        @Override
        public void onBindViewHolder(@NonNull RowHolder holder, int position) {
            holder.bindModel(animalsNames[position]);
        }

        @Override
        public int getItemCount() {
            return (animalsNames.length);
        }
    }

    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView label=null;
        ImageView icon=null;

        public RowHolder(@NonNull View row) {
            super(row);
            row.setOnClickListener(this);
            label = (TextView)row.findViewById(R.id.animalNameRow);
            icon = (ImageView)row.findViewById(R.id.animalImg);
        }

        @Override
        public void onClick(View v) {
            final String clickedAnimal = label.getText().toString();

            Intent intent = new Intent(MainActivityRecycleView.this, ClickedAnimal.class);
            //stock animalName
            intent.putExtra("animalName", clickedAnimal);
            startActivity(intent);
        }

        void bindModel(String item) {
            label.setText(item);
            final Animal animal = AnimalList.getAnimal(item);
            Resources res = getResources();
            String imgFile = animal.getImgFile();
            int resID = res.getIdentifier(imgFile , "drawable", getPackageName());
            icon.setImageResource(resID);
        }


    }
}


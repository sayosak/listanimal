package com.example.listanimals;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class ClickedAnimal extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clickedanimal);

        //Get animalName
        Intent intent = getIntent();
        String animalClickedName = intent.getStringExtra("animalName");

        final TextView animalName = findViewById(R.id.animalName);
        animalName.setText(animalClickedName);

        //get the animal from the HashMap
        final Animal animal = AnimalList.getAnimal(animalClickedName);

        //set the image
        final ImageView animalImage = findViewById(R.id.animalImage);

        Resources res = getResources();
        String imgFile = animal.getImgFile();
        int resID = res.getIdentifier(imgFile , "drawable", getPackageName());
        animalImage.setImageResource(resID);

        //set the informations
        final TextView esperance = findViewById(R.id.esperance);
        final TextView gestation = findViewById(R.id.gestation);
        final TextView poidsNaissance = findViewById(R.id.poidsNaissance);
        final TextView poidsAdulte = findViewById(R.id.poidsAdulte);
        final EditText conservation = findViewById(R.id.conservation);

        esperance.setText(animal.getStrHightestLifespan());
        gestation.setText(animal.getStrGestationPeriod());
        poidsNaissance.setText(animal.getStrBirthWeight());
        poidsAdulte.setText(animal.getStrAdultWeight());
        conservation.setText(animal.getConservationStatus());

        //button sauvegarder
        final Button btnSave = findViewById(R.id.btnSauvegarder);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String modifiedConservation = conservation.getText().toString();
                if (!animal.getConservationStatus().equals(modifiedConservation)){
                    animal.setConservationStatus(modifiedConservation);
                }
                finish();
            }
        });
    }
}
